using UnityEngine;
using System.Collections;

public class SelectImage : MonoBehaviour {
	
	public int pos;
	private string text;
	bool fullScreen;
	static bool globalFullScreen = false;
	float timeStart;
	
	public Material ownMaterial;
	public Material material1;
	public Material material2;
	public Material material3;
	public Transform subImage;
	
	// Use this for initialization
	void Start () {
		ownMaterial.color = Color.black;
		fullScreen = false;
		timeStart = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (fullScreen && Input.GetKey(KeyCode.Space) && (Time.time - timeStart) > 0.1f)
		{
			timeStart = Time.time;
			fullScreen = false;
			globalFullScreen = false;
		}
		else if (!globalFullScreen && ownMaterial.color == Color.green && Input.GetKey(KeyCode.Space) && (Time.time - timeStart) > 0.1f)
		{
			timeStart = Time.time;
			fullScreen = true;
			globalFullScreen = true;
		}
	}
	
	void OnMouseDown()
	{
		if (!globalFullScreen)
		{
			ManageGallery manage = GameObject.Find("MainCamera").GetComponent("ManageGallery") as ManageGallery;
			text = manage.GetFilename(pos);
			DeleteObject del = GameObject.Find("Delete").GetComponent("DeleteObject") as DeleteObject;
			del.selectedImage = text;
			ownMaterial.color = Color.green;
			material1.color = Color.black;
			material2.color = Color.black;
			material3.color = Color.black;
		}
	}
	
	void OnGUI()
	{
		if (fullScreen)
		{
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), renderer.sharedMaterial.mainTexture, ScaleMode.StretchToFill);
		}
	}
	
	public bool GetGlobalFullScreen()
	{
		return globalFullScreen;
	}
}
