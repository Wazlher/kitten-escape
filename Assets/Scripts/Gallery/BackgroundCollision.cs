﻿using UnityEngine;
using System.Collections;

public class BackgroundCollision : MonoBehaviour {

	public Material[] mat;
	
	void OnMouseDown()
	{
		SelectImage img = GameObject.Find("Image 1").GetComponent("SelectImage") as SelectImage;
		if (img.GetGlobalFullScreen() == false)
		{
			DeleteObject del = GameObject.Find("Delete").GetComponent("DeleteObject") as DeleteObject;
			del.selectedImage = null;
			foreach (Material m in mat)
				m.color = Color.black;
		}
	}
}
