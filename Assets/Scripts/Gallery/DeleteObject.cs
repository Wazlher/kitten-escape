﻿using UnityEngine;
using System.Collections;
using System.IO;

public class DeleteObject : MonoBehaviour {
	
	public string selectedImage;
	private bool doUpdate;
	
	// Use this for initialization
	void Start () {
		selectedImage = null;
		doUpdate = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (doUpdate)
		{
			if (selectedImage != null)
				renderer.material.color = Color.white;
			else if (selectedImage == null)
				renderer.material.color = Color.grey;
		}
	}
	
	void OnMouseDown()
	{
		SelectImage img = GameObject.Find("Image 1").GetComponent("SelectImage") as SelectImage;
		if (img.GetGlobalFullScreen() == false && selectedImage != null)
		{
			File.Delete(selectedImage);
			File.Delete(selectedImage + ".meta");
			ManageGallery manage = GameObject.Find("MainCamera").GetComponent("ManageGallery") as ManageGallery;
			manage.deleteScreenshot(selectedImage);
		}
	}
	
	void OnMouseEnter()
	{
		if (selectedImage != null)
		{
			doUpdate = false;
			renderer.material.color = Color.black;
		}
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
		doUpdate = true;
	}
}
