﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class ManageGallery : MonoBehaviour {
	
	private List<FileInfo> info;
	private List<WWW> textures;
	private Texture2D basicTex;
	public Material image1;
	public Material image2;
	public Material image3;
	public Material image4;
	private int number;
	
	// Use this for initialization
	void Start () {
		DirectoryInfo dir = new DirectoryInfo(Application.dataPath + "/../KittenEscape_Screenshots");
		FileInfo[] infos = dir.GetFiles("*.jpg");
		basicTex = Resources.Load("Screenshots/basicscreen", typeof(Texture2D)) as Texture2D;
		info = new List<FileInfo>(infos);
		number = 0;
		textures = new List<WWW>();
		foreach (FileInfo f in info)
		{
			LoadImage(f);
			number++;
		}
		ChangeImages(1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void LoadImage(FileInfo f)
	{
		string url = new UriBuilder(f.FullName).Uri.AbsoluteUri;
		textures.Add(new WWW(url));
	}
	
	void ChangeImage(Material image, int id)
	{
		if (id < number)
			image.mainTexture = textures[id].texture;
		else
			image.mainTexture = basicTex;
	}
	
	public void ChangeImages(int id)
	{
		id--;
		ChangeImage(image1, id);
		ChangeImage(image2, id + 1);
		ChangeImage(image3, id + 2);
		ChangeImage(image4, id + 3);
	}
	
	public int GetScreenshotNumber()
	{
		return number;
	}
	
	public string GetFilename(int id)
	{
		ManageArrows manage = GameObject.Find("Arrow left").GetComponent("ManageArrows") as ManageArrows;
		string ret;
		try
		{
			ret = info[id + manage.GetPos() - 1].FullName;
		}
		catch (System.ArgumentOutOfRangeException e)
		{
			ret = null;
		}
		return ret;
	}
	
	public void deleteScreenshot(string filename)
	{
		int i;
		for (i = 0; filename != info[i].FullName; ++i)	;
		int rest = (i % 4) - 1;
		textures.RemoveAt(i);
		info.RemoveAt(i);
		
		number--;
		DeleteObject del = GameObject.Find("Delete").GetComponent("DeleteObject") as DeleteObject;
        if (number >= 1)
            del.selectedImage = info[i].FullName;
        else
            del.selectedImage = null;
		ChangeImages(i - rest);
	}
}
