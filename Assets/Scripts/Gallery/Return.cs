﻿using UnityEngine;
using System.Collections;

public class Return : MonoBehaviour {

	void OnMouseDown()
	{
		SelectImage img = GameObject.Find("Image 1").GetComponent("SelectImage") as SelectImage;
		if (img.GetGlobalFullScreen() == false)
			CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuMain"); } );
	}
	
	void OnMouseEnter()
	{
		renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
	}
}
