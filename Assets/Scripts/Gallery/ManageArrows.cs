using UnityEngine;
using System.Collections;

public class ManageArrows : MonoBehaviour {
	
	private static int pos;
	public string arrow;
	public Material[] mat;
	
	void Start () {
		pos = 1;
	}
	
	void OnMouseDown()
	{
		audio.Play();
		SelectImage img = GameObject.Find("Image 1").GetComponent("SelectImage") as SelectImage;
		if (img.GetGlobalFullScreen() == false)
		{
			ManageGallery manage = GameObject.Find("MainCamera").GetComponent("ManageGallery") as ManageGallery;
			int number = manage.GetScreenshotNumber();
			print (number);
			if (arrow == "left" && pos > 1)
			{
				pos -= 4;
				manage.ChangeImages(pos);
				ResetNewPage();
			}
			else if (arrow == "right" && pos <= number - 4)
			{
				pos += 4;
				manage.ChangeImages(pos);
				ResetNewPage();
			}
		}
	}
	
	public int	GetPos()
	{
		return pos;
	}
	
	void ResetNewPage()
	{
		DeleteObject del = GameObject.Find("Delete").GetComponent("DeleteObject") as DeleteObject;
		del.selectedImage = null;
		foreach (Material m in mat)
			m.color = Color.black;
	}
}
