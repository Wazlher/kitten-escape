﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Validate : MonoBehaviour {

	public TextMesh validate;
	public GameObject prefab;
	public TextMesh screenshot;
	public AudioClip audioWin;
	public AudioClip audioLose;
	
	void Start()
	{
		Screen.showCursor = true;
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null)
		{
			if (GS.victory)
			{
				GS.audio.clip = audioWin;
				GS.audio.Play ();
			}
			else
			{
				GS.audio.clip = audioLose;
				GS.audio.PlayOneShot(audioLose);
			}
		}
	}
	
	void OnMouseEnter()
	{
		validate.renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		validate.renderer.material.color = Color.white;
	}
	
	void OnMouseDown()
	{
		audio.Play();
		Time.timeScale = 1.0f;
		Destroy (prefab);
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (screenshot.text == "No")
			DeleteScreenshot(GS);
		if (GS != null)
		{
			GS.StartEnding();
			print ("Je passe");
		}
		Screen.showCursor = false;
	}
	
	void DeleteScreenshot(GameSystem GS)
	{
		foreach (string s in GS.screenshotPath)
		{
			print(s);
			File.Delete(s);
		}
	}
}
