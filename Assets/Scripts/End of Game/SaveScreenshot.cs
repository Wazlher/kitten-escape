﻿using UnityEngine;
using System.Collections;

public class SaveScreenshot : MonoBehaviour {
	
	public TextMesh saveValue;
	
	void Start () {
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null && (!GS.victory || !GS.screenshot))
		{
			saveValue.renderer.material.color = Color.gray;
			saveValue.text = "No";
		}
		
	}
	
	void OnMouseEnter()
	{
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null && GS.victory)
			saveValue.renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null && GS.victory)
			saveValue.renderer.material.color = Color.white;
	}
			
	void OnMouseDown()
	{
		audio.Play();
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null && GS.victory)
		{
			if (saveValue.text == "Yes")
				saveValue.text = "No";
			else
				saveValue.text = "Yes";
		}
	}
}
