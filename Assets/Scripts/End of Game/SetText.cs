﻿using UnityEngine;
using System.Collections;

public class SetText : MonoBehaviour {
	
	public TextMesh text1;
	public TextMesh text2;
	
	// Use this for initialization
	void Start () {
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null && GS.endOfGame == true)
		{
			if (GS.victory)
			{
				text1.text = "Congratulations !";
				text2.text = "You won !";
			}
			else
			{
				text1.text = "Sorry";
				text2.text = "You lost...";
			}
		}
	}
}
