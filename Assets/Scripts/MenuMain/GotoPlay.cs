﻿using UnityEngine;
using System.Collections;

public class GotoPlay : MonoBehaviour {
	
	void OnMouseDown()
	{
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuSelectLevel"); } );
	}
}
