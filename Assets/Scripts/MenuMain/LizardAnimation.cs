﻿using UnityEngine;
using System.Collections;

public class LizardAnimation : MonoBehaviour {
	
	public AudioClip music;
	
	void Start()
	{
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		GS.audio.clip = music;
		GS.audio.Play();
		this.animation.wrapMode = WrapMode.Loop;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
