﻿using UnityEngine;
using System.Collections;

public class MoveMenu : MonoBehaviour {
	
	public string animationForward;
	public string animationBackward;
	private static MoveMenu activedMenu = null;
	
	void OnMouseEnter()
	{
		this.AnimateForward();
	}
	
	void OnMouseExit()
	{
		this.AnimateBackward();
	}
	
	void AnimateForward()
	{
		if (activedMenu)
			activedMenu.AnimateBackward();
		animation.Play(animationForward);
		audio.Play();
		activedMenu = this;
	}
	
	void AnimateBackward()
	{
		animation.Play(animationBackward);
		audio.Stop();
		activedMenu = null;
	}
}
