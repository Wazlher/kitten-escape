﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(SplashscreenWait());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
			CameraFade.StartAlphaFade(Color.black, false, 7.0f, 0.0f, () => { Application.LoadLevel("MenuMain"); } );
	}
	
	IEnumerator SplashscreenWait()
	{
		yield return new WaitForSeconds(3);
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuMain"); } );
	}
}