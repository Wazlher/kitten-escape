using UnityEngine;
using System.Collections;

public class ManageScreenshot : MonoBehaviour {

	public TextMesh text;
	
	void Start()
	{
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		bool val = GS.screenshot;
		if (val)
			text.text = "On";
		else
			text.text = "Off";
		
	}
	
	void OnMouseDown()
	{
		this.audio.Play();
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (text.text == "On")
		{
			GS.screenshot = false;
			text.text = "Off";
		}
		else
		{
			GS.screenshot = true;
			text.text = "On";
		}
	}
	
	void OnMouseEnter()
	{
		renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
	}
}
