using UnityEngine;
using System.Collections;

public class ManageReturn : MonoBehaviour {

	void OnMouseDown()
	{
		this.audio.Play ();
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuMain"); } );
	}
	
	void OnMouseEnter()
	{
		renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
	}
}
