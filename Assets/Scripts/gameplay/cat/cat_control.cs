﻿using UnityEngine;
using System.Collections;

public class cat_control : MonoBehaviour 
{
	public GameObject[]		    targets;
    public GameObject           player;
	public GameObject		    stun_part;
	public Transform		    Spawn;
    public int                  pause;
	public int				    stun = 0;
    public float                normalSpeed;

	private NavMeshAgent	    agent;
    private controller_player   playerController;
	private GameObject		    Part_tmp;
	private float			    posx = 0;
	private float			    posz = 0;
	private float			    tposx = 0;
	private float			    tposz = 0;
	private int				    run;
	private bool			    move = true;
	private bool			    flee = false;

	void Start () 
	{
        playerController = player.GetComponent<controller_player>();
        NewTarget();
	}

    void NewTarget()
    {
        if (targets.Length == 0 || targets.Length - 1 == 0)
            return;
        else if (targets.Length - 1 == 1)
        {
            run = (run + 1) % 2;
            return;
        }

        int newRun = Random.Range(0, targets.Length - 1);

        if (newRun == run)
            NewTarget();
        else
            run = newRun;
    }

	void Update () 
	{
		if (stun == 0)
        {
            CheckPlayerPosition();
			posz = this.transform.position.z;
			posx = this.transform.position.x;
			agent = GetComponent<NavMeshAgent>();
            if (targets.Length > 0)
            {
                move = MoveCat();
                if (!move)
                {
                    NotMove();
                    run = 0;
                }
            }
			flee = FleePlayer();
		}
		else
		{
			stun++;
			if (stun >= 100)
			{
				Destroy(Part_tmp, 2.0f);
				stun = 0;
			}
		}
	}

    void CheckPlayerPosition()
    {
        if (transform.position.x + 1 >= player.transform.position.x && transform.position.x - 1 <= player.transform.position.x &&
            transform.position.z + 1 >= player.transform.position.z && transform.position.z - 1 <= player.transform.position.z &&
            player.GetComponent<controller_player>().isRunning)
            StunCat();
    }

    void StunCat()
    {
        audio.Play();
        stun++;
        this.animation.Stop();
        Part_tmp = Instantiate(this.stun_part, Spawn.position, Spawn.rotation) as GameObject;
        agent.SetDestination(transform.position);
    }

    bool MoveCat()
    {
        if (targets[run] != null)
        {
            tposx = targets[run].transform.position.x;
            tposz = targets[run].transform.position.z;
            agent.SetDestination(targets[run].transform.position);
        }
        else
            return false;
        if ((tposz <= posz + 10 && tposz >= posz - 10) &&
            (tposx <= posx + 10 && tposx >= posx - 10))
        {
            this.animation.Play("A_idle");
            pause++;
            if (flee)
                NewTarget();
            if (pause > 250)
                this.animation.Stop();
            if (!this.animation.isPlaying)
            {
                pause = 0;
                NewTarget();
            }
        }
        else
            this.animation.Play("A_run");
        return true;
    }

    void NotMove()
    {
        this.animation.Play("A_idle");
    }

    bool FleePlayer()
    {
        if (transform.position.x + 5 >= player.transform.position.x && transform.position.x - 5 <= player.transform.position.x &&
            transform.position.y + 5 >= player.transform.position.y && transform.position.y - 5 <= player.transform.position.y &&
            transform.position.z + 5 >= player.transform.position.z && transform.position.z - 5 <= player.transform.position.z)
        {
            agent.speed = normalSpeed * 3;
            this.animation.Play("A_run");
            return true;
        }
        else
        {
            agent.speed = normalSpeed;
            return false;
        }
    }

	void OnCollisionEnter(Collision c)
	{
        if (c.gameObject.name == "Bullet(Clone)")
            StunCat();
	}
}