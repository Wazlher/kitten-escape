﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour
{
	private bool	turn_left = false;
	private bool	turn_right = false;
	private bool	turn_top = true;
	private bool	turn_bot = false;
    private bool    jump = false;
    private int     i = 0;

	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (jump)
            this.animation.Stop();
        if (Input.GetKey(KeyCode.Space) || jump == true)
        {
            jump = true;
            if (i < 100)
            {
                if (i > 49)
                {
                    this.transform.Translate(0.0f, -0.1f, 0.1f);
                    //Camera.main.transform.Translate(0.0f, 0.0f, 0.1f);
                }
                else
                {
                    this.transform.Translate(0.0f, 0.1f, 0.1f);
                    //Camera.main.transform.Translate(0.0f, 0.0f, 0.1f);
                }
                i++;
            }
            else
            {
                i = 0;
                jump = false;
            }
        }

        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (turn_left == false && turn_right == true)
            {
                this.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Translate(0.0f, 0.0f, -13.0f);
            }
            else if (turn_left == false && turn_right == false && turn_top == true)
            {
                this.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Translate(7.0f, 0.0f, -7.0f);
            }
            else if (turn_left == false && turn_right == false && turn_bot == true)
            {
                this.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Translate(-6.0f, 0.0f, -7.0f);
            }
            turn_left = true;
            turn_right = false;
            turn_top = false;
            turn_bot = false;
            this.transform.Translate(0.0f, 0.0f, 0.1f);
            //Camera.main.transform.Translate(0.0f, 0.0f, 0.1f);
            if (!jump)
                this.animation.Play();
        }

        else if (Input.GetKey(KeyCode.RightArrow))
        {
            if (turn_left == true && turn_right == false)
            {
                this.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Translate(0.0f, 0.0f, -13.0f);
            }
            else if (turn_left == false && turn_right == false && turn_top == true)
            {
                this.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Translate(-6.0f, 0.0f, -7.0f);
            }
            else if (turn_left == false && turn_right == false && turn_bot == true)
            {
                this.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Translate(7.0f, 0.0f, -7.0f);
            }
            turn_left = false;
            turn_right = true;
            turn_top = false;
            turn_bot = false;
            this.transform.Translate(0.0f, 0.0f, 0.1f);
            //Camera.main.transform.Translate(0.0f, 0.0f, 0.1f);
            if (!jump)
                this.animation.Play();
        }

        else if (Input.GetKey(KeyCode.UpArrow))
        {
            if (turn_bot == true && turn_top == false)
            {
                this.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Translate(0.0f, 0.0f, -13.0f);
            }
            else if (turn_bot == false && turn_top == false && turn_right == true)
            {
                this.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Translate(7.0f, 0.0f, -7.0f); 
            }
            else if (turn_bot == false && turn_top == false && turn_left == true)
            {
                this.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Translate(-6.0f, 0.0f, -7.0f);
            }
            turn_left = false;
            turn_right = false;
            turn_top = true;
            turn_bot = false;
            this.transform.Translate(0.0f, 0.0f, 0.1f);
            //Camera.main.transform.Translate(0.0f, 0.0f, 0.1f);
            if (!jump)
                this.animation.Play();
        }

        else if (Input.GetKey(KeyCode.DownArrow))
        {
            if (turn_bot == false && turn_top == true)
            {
                this.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 180.0f, 0.0f);
                //Camera.main.transform.Translate(0.0f, 0.0f, -13.0f);
            }
            else if (turn_bot == false && turn_top == false && turn_right == true)
            {
                this.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, 90.0f, 0.0f);
                //Camera.main.transform.Translate(-6.0f, 0.0f, -7.0f);
            }
            else if (turn_bot == false && turn_top == false && turn_left == true)
            {
                this.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Rotate(0.0f, -90.0f, 0.0f);
                //Camera.main.transform.Translate(7.0f, 0.0f, -7.0f);
            }
            turn_left = false;
            turn_right = false;
            turn_top = false;
            turn_bot = true;
            this.transform.Translate(0.0f, 0.0f, 0.1f);
            //Camera.main.transform.Translate(0.0f, 0.0f, 0.1f);
            if (!jump)
                this.animation.Play();
        }
        else
            this.animation.Stop();
	}
}
