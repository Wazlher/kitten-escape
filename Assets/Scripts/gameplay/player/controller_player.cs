﻿using UnityEngine;
using System.Collections;

public class controller_player : MonoBehaviour
{
    public float movementSpeed = 5.0f;
    public float mouseSensitivity = 5.0f;
    public float jumpSpeed = 20.0f;
    public Camera CameraMain;
    public Camera CameraFPS;
    public GameObject Stone_part;
    public Transform Spawn;
    public float upDownRange = 60.0f;
    public int stop = 0;
    public int pause;
    public bool move = true;
    public bool isRunning = false;

    private GameObject Part_tmp;
    private float verticalRotation = 0;
    private float gravity = 9.8f;
    private float posx;
    private float posz;
    private float verticalVelocity = 0;
    private CharacterController characterController;

    void Start()
    {
        Screen.lockCursor = true;
        characterController = GetComponent<CharacterController>();
        pause = 0;
    }

    void Update()
    {
        CheckSpeed();

        if (move)
        {
            posx = this.transform.position.x;
            posz = this.transform.position.z;
            float rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
            transform.Rotate(0, rotLeftRight, 0);
            verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
            verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
            if (CameraMain.enabled)
            {
                CameraMain.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

                float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
                float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;
                verticalVelocity -= gravity * Time.deltaTime * 12;
                if (characterController.isGrounded && Input.GetButton("Jump"))
                    verticalVelocity = jumpSpeed;

                Vector3 speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);
                speed = transform.rotation * speed;
                characterController.Move(speed * Time.deltaTime);
                if (posx != this.transform.position.x && posz != this.transform.position.z)
                    this.animation.Play("chaplinwalk");
                else
                {
                    if (pause >= 250)
                    {
                        this.animation.Play("pathsala");
                        pause = 0;
                    }

                    if (!this.animation.IsPlaying("pathsala"))
                    {
                        this.animation.Stop();
                        pause++;
                    }
                }
            }
            else
                CameraFPS.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);
        }
        else if (!animation.IsPlaying("Slip"))
        {
            Destroy(Part_tmp, 2.0f);
            move = true;
        }
    }

    void LateUpdate()
    {
        if (!move)
            return;

        GameObject[] ennemies = GameObject.FindGameObjectsWithTag("KittyFan");

        foreach (GameObject e in ennemies)
        {
            if (e.GetComponent<KittyFan_control>().touch)
            {
                CheckCamera();
                this.animation.Stop();
                move = false;
                Part_tmp = Instantiate(this.Stone_part, Spawn.position, Spawn.rotation) as GameObject;
                animation.Play("Slip");
                return;
            }
        }
    }

    void CheckSpeed()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            movementSpeed *= 2;
            animation["chaplinwalk"].speed *= 2;
            isRunning = true;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            movementSpeed /= 2;
            animation["chaplinwalk"].speed /= 2;
            isRunning = false;
        }
    }

    void CheckCamera()
    {
        if (CameraFPS.enabled)
        {
            CameraFPS.enabled = false;
            CameraMain.enabled = true;
        }
    }
}