﻿using UnityEngine;
using System.Collections;

public class AggroZone : MonoBehaviour 
{

	public bool enter = false;
	
	void OnTriggerStay(Collider c)
	{
		if (c.tag == "Player")
			enter = true;
	}
	
	void OnTriggerExit(Collider c)
	{
		if (c.tag == "Player")
			enter = false;
	}
}
