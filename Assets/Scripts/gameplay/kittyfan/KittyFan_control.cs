﻿using UnityEngine;
using System.Collections;

public class KittyFan_control : MonoBehaviour 
{
	public GameObject[]		targets;
    public GameObject       player;
    public GameObject       zone;
    public int              pause;
    public bool             touch = false;

	private NavMeshAgent	agent;
    private AggroZone       aggroZone;
	private float			tposx = 0;
	private float			tposz = 0;
	private int				run;
	bool					move = false;
	bool					attack = false;

	void Start () 
	{
        aggroZone = zone.GetComponent<AggroZone>();
        NewTarget();
	}

    void NewTarget()
    {
        if (targets.Length == 0 || targets.Length - 1 == 0)
            return;
        else if (targets.Length - 1 == 1)
        {
            run = (run + 1) % 2;
            return;
        }

        int newRun = Random.Range(0, targets.Length - 1);

        if (newRun == run)
            NewTarget();
        else
            run = newRun;
    }

    bool HitPlayer()
    {
        if (player.transform.position.x <= transform.position.x + 1 && player.transform.position.x >= transform.position.x - 1 &&
            player.transform.position.y <= transform.position.y + 1 && player.transform.position.y >= transform.position.y - 1 &&
            player.transform.position.z <= transform.position.z + 1 && player.transform.position.z >= transform.position.z - 1)
            return true;
        else
            return false;
    }

	bool	AttackPlayer()
	{
        if (HitPlayer())
		{
            audio.Play();
			touch = true;
			return false;
		}
        else
            touch = false;

        if (!touch && !player.animation.IsPlaying("Slip"))
		{
            if (aggroZone.enter)
            {
                agent.SetDestination(player.transform.position);
                this.animation.Play("run");
                return true;
            }
            else
                return false;
        }
        return false;
    }

    void	NotMove()
    {
    	this.animation.Play("idle");
    }

    bool	MoveKittyFan()
    {
        if (targets[run] != null)
        {
            tposx = targets[run].transform.position.x;
            tposz = targets[run].transform.position.z;
            agent.SetDestination(targets[run].transform.position);
        }
        else
        {
            NewTarget();
            return false;
        }

        if ((transform.position.z >= tposz - 10 && transform.position.z <= tposz + 10) &&
            (transform.position.x >= tposx - 10 && transform.position.x <= tposx + 10))
        {
            this.animation.Play("idle");
            pause++;
            if (pause > 250)
            {
                this.animation.Stop();
                pause = 0;
                NewTarget();
            }
        }
        else
            this.animation.Play("run");
        return true;
    }

    void Update () 
    {
        if (targets.Length == 0)
            animation.Play("idle");
        agent = GetComponent<NavMeshAgent>();
        if ((attack == false || touch == true) && targets.Length > 0)
        {
            move = MoveKittyFan();
            if (move == false)
            {
                NotMove();
                run = 0;
            }
        }
        attack = AttackPlayer();
    }
}