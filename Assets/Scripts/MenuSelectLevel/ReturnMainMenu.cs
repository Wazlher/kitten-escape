using UnityEngine;
using System.Collections;

public class ReturnMainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown()
	{
		audio.Play();
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuMain"); } );
	}
}
