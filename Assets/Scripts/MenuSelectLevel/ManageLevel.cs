﻿using UnityEngine;
using System.Collections;
using System;

public class ManageLevel : MonoBehaviour {

	public static int id;
	private string dir;
	public TextMesh levelName;
    public TextMesh kittenToCatch;
    public TextMesh timer;
	
	void Start()
	{
		dir = Application.dataPath + "/Resources/Levels/";
		id = 1;
		ChangeImage();
	}
	
	void Update()
	{
	}
	
	void SetPos(int i)
	{
		id += i;
		if (id > 2)
			id = 1;
		else if (id < 1)
			id = 2;
		levelName.text = "Level 00" + id;
        GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
        GS.ChangeLevel(id);
        kittenToCatch.text = GS.kittenTotal.ToString();
        timer.text = GS.timerLevel.ToString() + " minutes";
		ChangeImage();
	}
	
	void ChangeImage()
	{
		renderer.material.mainTexture = Resources.Load("Levels/Level " + id, typeof(Texture2D)) as Texture2D;
	}
}
