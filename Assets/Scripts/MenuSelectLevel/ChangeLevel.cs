using UnityEngine;
using System.Collections;

public class ChangeLevel : MonoBehaviour {
	
	public string text;
		
	void	OnMouseDown()
	{
		audio.Play();
		if (text == "right")
			GameObject.Find("ImageLevel").GetComponent ("ManageLevel").SendMessage ("SetPos", 1);
		else if (text == "left")
			GameObject.Find("ImageLevel").GetComponent ("ManageLevel").SendMessage ("SetPos", -1);
	}
	
	void	OnMouseEnter()
	{
		renderer.material.color = new Color(50.0f / 255.0f, 50.0f / 255.0f, 50.0f / 255.0f, 1.0f);
	}
	
	void	OnMouseExit()
	{
		renderer.material.color = new Vector4(212.0f / 255.0f, 212.0f / 255.0f, 212.0f / 255.0f, 1.0f);
	}
}
