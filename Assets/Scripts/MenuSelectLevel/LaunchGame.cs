using UnityEngine;
using System.Collections;

public class LaunchGame : MonoBehaviour {
	
	public AudioClip music;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown()
	{
		audio.Play();
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("Game"); } );
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		GS.music.clip = music;
		GS.music.Play();
	}
}
