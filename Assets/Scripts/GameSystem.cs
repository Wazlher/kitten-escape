﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class GameSystem : MonoBehaviour {
	
	private static bool created = false;
	
	public AudioSource music;
	
	/* Settings */
	public float musicVolume;
	public float effectVolume;
	public bool screenshot;
	
	/* Game - Party */
	public int kittenTotal;
	public int kittenCatched = 0;
	public List<string> screenshotPath;
    public int timerLevel;
	
	/* Game -> End of Game */
	public bool endOfGame;
	public bool victory;
	public GameObject menuEnd;
	public AudioClip musicMenu;
	
	
	// Use this for initialization
	void Start () {
		if (!Directory.Exists(Application.dataPath + "/../KittenEscape_Screenshots"))
		{
			Directory.CreateDirectory(Application.dataPath + "/../KittenEscape_Screenshots");
		}
        ChangeLevel(1);
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void Awake()
	{
		if (!created)
		{
			Object.DontDestroyOnLoad(transform.gameObject);
			created = true;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
	
	public void StartEndOfGame(bool won)
	{
		endOfGame = true;
		/* Changer Victory par la bonne valeur */
		victory = won;
		GameObject menu = Instantiate(menuEnd) as GameObject;
		GameObject cam = GameObject.Find("Main Camera") as GameObject;
		menu.transform.position = cam.transform.position;
		Time.timeScale = 0.0f;
	//	menu.camera.enabled = true;
	}
	
	public void StartEnding()
	{
		CameraFade.StartAlphaFade(Color.black, false, 3.5f);
		GameObject cam = GameObject.Find("End Camera") as GameObject;
		cam.GetComponent<Camera>().enabled = true;
		GameObject player = GameObject.Find ("PlayerV2") as GameObject;
		player.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
		if (victory)
		{
			StartCoroutine("HappyEnd");
		}
		else
		{
			StartCoroutine("BadEnd");
		}
	}
	
	IEnumerator HappyEnd()
	{
		GameObject Ovni = GameObject.Find("OVNI") as GameObject;
		Ovni.animation.Play("ShipElevation");
		yield return new WaitForSeconds(Ovni.animation["ShipElevation"].length + 0.4f);
		Screen.showCursor = true;
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuSelectLevel"); } );
		music.clip = musicMenu;
		audio.Play ();
        kittenCatched = 0;
	}
	
	IEnumerator BadEnd()
	{
		OvniExplosion OE = GameObject.Find("OvniHelper").GetComponent("OvniExplosion") as OvniExplosion;
		OE.StartExplosion();
		yield return new WaitForSeconds(7.0f);
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuSelectLevel"); } );
		Screen.showCursor = true;
		music.clip = musicMenu;
		audio.Play ();
        kittenCatched = 0;
	}

    public void ChangeLevel(int id)
    {
        if (id == 1)
        {
            kittenTotal = 5;
            timerLevel = 10;
        }
        if (id == 2)
        {
            kittenTotal = 7;
            timerLevel = 3;
        }
    }
}
