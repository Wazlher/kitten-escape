﻿using UnityEngine;
using System.Collections;

public class Continue : MonoBehaviour {
	
	public GameObject prefab;
	
	void OnMouseEnter()
	{
		renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
	}
	
	void OnMouseDown()
	{
		Destroy(prefab);
		Time.timeScale = 1.0f;
		Screen.showCursor = false;
	}
}
