﻿using UnityEngine;
using System.Collections;

public class KittenCount : MonoBehaviour {
	
	public TextMesh catched;
	public TextMesh left;

	void Start () {
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null)
		{
			print ("Kitten catched = " + GS.kittenCatched + " \\ AND Kitten total = " + GS.kittenTotal);
			catched.text = (GS.kittenCatched).ToString();
			left.text = (GS.kittenTotal - GS.kittenCatched).ToString();
		}
	}
}
