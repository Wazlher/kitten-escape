﻿using UnityEngine;
using System.Collections;

public class EffectArrow : MonoBehaviour {

	public TextMesh text;
	public string side;
	
	void Start()
	{
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null)
			text.text = (GS.effectVolume).ToString();
	}
	
	void OnMouseDown()
	{
		float val = float.Parse(text.text);
		if (side == "left")
			val -= 10.0f;
		else if (side == "right")
			val += 10.0f;
		if (val > 100.0f)
			val = 100.0f;
		else if (val < 0.0f)
			val = 0.0f; 
		AudioSource[] audios = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		foreach (AudioSource audio in audios)
				audio.volume = val / 100.0f;
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (GS != null)
		{
			GS.effectVolume = val;
			GS.music.volume = GS.musicVolume / 100.0f;
		}
		text.text = val.ToString();
	}
	
	void	OnMouseEnter()
	{
		renderer.material.color = new Color(50.0f / 255.0f, 50.0f / 255.0f, 50.0f / 255.0f, 1.0f);
	}
	
	void	OnMouseExit()
	{
		renderer.material.color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}
}
