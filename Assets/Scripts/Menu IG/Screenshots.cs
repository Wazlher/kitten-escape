﻿using UnityEngine;
using System.Collections;

public class Screenshots : MonoBehaviour {
	
	public TextMesh text;
	
	void Start()
	{
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		bool val = GS.screenshot;
		if (val)
			text.text = "On";
		else
			text.text = "Off";
	}
	
	void OnMouseEnter()
	{
		renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
	}
	
	void OnMouseDown()
	{
		Time.timeScale = 1.0f;
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		if (text.text == "On")
		{
			GS.screenshot = false;
			text.text = "Off";
		}
		else
		{
			GS.screenshot = true;
			text.text = "On";
		}
		Time.timeScale = 0.0f;
	}
}
