﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SelectLevel : MonoBehaviour {
	
	public AudioClip music;
	
	void OnMouseEnter()
	{
		renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
	}
	
	void OnMouseDown()
	{
		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
        GS.kittenCatched = 0;
		GS.audio.clip = music;
		foreach (string s in GS.screenshotPath)
		{
			print(s);
			File.Delete(s);
		}
		GS.audio.Play();
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("MenuSelectLevel"); } );
	}
}
