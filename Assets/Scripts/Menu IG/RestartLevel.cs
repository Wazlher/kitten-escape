﻿using UnityEngine;
using System.Collections;
using System.IO;

public class RestartLevel : MonoBehaviour {

	void OnMouseEnter()
	{
		renderer.material.color = Color.black;
	}
	
	void OnMouseExit()
	{
		renderer.material.color = Color.white;
	}
	
	void OnMouseDown()
	{
        GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
        GS.kittenCatched = 0;
		GS.audio.Play();
		foreach (string s in GS.screenshotPath)
		{
			print(s);
			File.Delete(s);
		}
		Time.timeScale = 1.0f;
		CameraFade.StartAlphaFade(Color.black, false, 3.0f, 0.0f, () => { Application.LoadLevel("Game"); } );
	}
}
