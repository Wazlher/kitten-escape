﻿using UnityEngine;
using System.Collections;

public class ShipLanding : MonoBehaviour {
	
	private bool animationEnd = false;
	public Camera cutscene;
	public Camera maincam;
	public Camera minimap;
	public Transform character;
	public Transform spawnPoint;
	
	void Start ()
    {
        Camera[] cameras = Camera.allCameras;

        foreach (Camera c in cameras)
            c.enabled = false;
        cutscene.enabled = true;
        animation.Play("ShipLanding");
	}

	void Update()
	{
		if (!animationEnd && !animation.IsPlaying("ShipLanding"))
		{
			StartCoroutine("Transition");
			maincam.enabled = true;
			minimap.enabled = true;
			cutscene.enabled = false;
			character.transform.position = spawnPoint.transform.position;
			animationEnd = true;
			GUI_Countdown timer = GameObject.Find("GUI").GetComponent("GUI_Countdown") as GUI_Countdown;
			timer.start = true;
		}
	}
	
	IEnumerator Transition()
	{
		CameraFade.StartAlphaFade(Color.black, true, 5.0f);
		yield return new WaitForSeconds(3.0f);
	}
}
