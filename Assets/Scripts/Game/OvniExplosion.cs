﻿using UnityEngine;
using System.Collections;

public class OvniExplosion : MonoBehaviour {
	
	public GameObject sparks;
	public GameObject mushroom;
	public GameObject simple;
	
	public GameObject ship;
	
	public float timerSparks;
	public float timerMushroom;
	public float timerSimple;
	private float timer;
	
	private bool sparksOn;
	private bool mushroomOn;
	private bool simpleOn;
	private bool endOfGame = false;
		
	// Use this for initialization
	void Start () {
		sparksOn = false;
		mushroomOn = false;
		simpleOn = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (endOfGame)
		{
			float currentTime = Time.time;
			if (!sparksOn && currentTime > timer + timerSparks)
			{
				GameObject s = Instantiate(sparks) as GameObject;
				s.transform.position = new Vector3(transform.position.x, 
												   transform.position.y,
												   transform.position.z);
				sparksOn = true;
			}
			if (!mushroomOn && currentTime > timer + timerMushroom)
			{
				GameObject m = Instantiate(mushroom) as GameObject;
				m.transform.position = new Vector3(transform.position.x, 
												   transform.position.y,
												   transform.position.z);
				mushroomOn = true;
			}
			if (!simpleOn && currentTime > timer + timerSimple)
			{
				GameObject s = Instantiate(simple) as GameObject;
				s.transform.position = new Vector3(transform.position.x,
												   transform.position.y,
												   transform.position.z);
				simpleOn = true;
				Destroy(ship);
			}
		}
	}
	
	public void StartExplosion()
	{
		timer = Time.time + 1.7f;
		endOfGame = true;
	}
}
