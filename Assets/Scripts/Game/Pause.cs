﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour
{
	public GameObject prefab;
	public Transform pos;
	public Camera pauseCamera;

    private GameObject menu = null;

	void Update () {
		if (menu == null && Input.GetKeyDown(KeyCode.P))
		{
            GameObject.FindGameObjectWithTag("Player").GetComponent<controller_player>().move = false;
			menu = Instantiate(prefab, pos.position, Quaternion.Inverse(pos.rotation)) as GameObject;
            pauseCamera.enabled = true;
            //pauseCamera.audio.enabled = false;
			Time.timeScale = 0.0f;
			Screen.showCursor = true;
		}
		else if (menu != null && Input.GetKeyDown(KeyCode.P))
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<controller_player>().move = true;
			Destroy(menu);
			Time.timeScale = 1.0f;
			Screen.showCursor = false;
		}
	}
}
