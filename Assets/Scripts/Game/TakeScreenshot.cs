﻿using UnityEngine;
using System.Collections;

public class TakeScreenshot : MonoBehaviour {

    private int screenshotCount = 0;
 //   private int resWidth = Screen.Width;
//    private int resHeight = Screen.Height;
 
    // Check for screenshot key each frame
    void Update()
    {
        // take screenshot on up->down transition of F12 key
        if (Input.GetKeyDown(KeyCode.F12))
        {
            TakeAScreenshot();
        }
    }

    public void TakeAScreenshot()
    {
        GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
        if (GS != null && GS.screenshot)
        {
            string screenshotFilename;
            do
            {
                screenshotCount++;
                screenshotFilename = Application.dataPath + "/../KittenEscape_Screenshots/screenshot" + screenshotCount + ".jpg";
            } while (System.IO.File.Exists(screenshotFilename));

            Application.CaptureScreenshot(screenshotFilename);
            audio.Play();
            GS.screenshotPath.Add(screenshotFilename);
        }
    }
}
