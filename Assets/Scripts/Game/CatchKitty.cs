﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CatchKitty : MonoBehaviour
{
    public float                catchDistance;

    private GameObject[]        kittens;
    private Camera              cameraFPS;

    void Start()
    {
        kittens = GameObject.FindGameObjectsWithTag("Kitty");
        cameraFPS = GameObject.FindGameObjectWithTag("CameraFPS").GetComponent<Camera>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && !cameraFPS.enabled)
        {
            int n = 0;

            foreach (GameObject kitty in kittens)
            {
                if (Vector3.Distance(kitty.transform.position, transform.position) < catchDistance)
                {
                    if (animation.isPlaying)
                        animation.Stop();
                    animation.CrossFade("funny");
                    Exterminate(n);
                }
                n++;
            }
        }
    }

    void Exterminate(int position)
    {
        List<GameObject>    tmp = new List<GameObject>(kittens);

        AudioSource AS = gameObject.AddComponent<AudioSource>();
        AS.clip = kittens[position].audio.clip;
        AS.Play();
        Destroy(kittens[position]);
        tmp.RemoveAt(position);
        kittens = tmp.ToArray();

		GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
		GS.kittenCatched++;
        TakeScreenshot TS = GameObject.Find("PlayerV2").GetComponent("TakeScreenshot") as TakeScreenshot;
        TS.TakeAScreenshot();
    }
}
