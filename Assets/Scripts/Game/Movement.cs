﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public float        movementSpeed   = 6.0f;
    public float        turnSmoothing   = 15.0f;   // A smoothing value for turning the player.

    private Vector3     jumpVec;

    void FixedUpdate()
    {
        // Cache the inputs.
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        MovementManagement(h, v);
    }

    void MovementManagement(float horizontal, float vertical)
    {
        if (horizontal != 0f || vertical != 0f)
        {
            Rotating(horizontal, vertical);

            if (Input.GetKey(KeyCode.LeftArrow))
                Translating(-horizontal);
            else if (Input.GetKey(KeyCode.RightArrow))
                Translating(horizontal);
            else if (Input.GetKey(KeyCode.UpArrow))
                Translating(vertical);
            else if (Input.GetKey(KeyCode.DownArrow))
                Translating(-vertical);
            else
                this.animation.Stop();
        }
    }

    void Rotating(float horizontal, float vertical)
    {
        // Create a new vector of the horizontal and vertical inputs.
        Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);

        // Create a rotation based on this new vector assuming that up is the global y axis.
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

        // Create a rotation that is an increment closer to the target rotation from the player's rotation.
        Quaternion newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);

        // Change the players rotation to this new rotation.
        rigidbody.MoveRotation(newRotation);
    }

    void Translating(float direction)
    {
        transform.position += direction * movementSpeed * transform.forward * Time.deltaTime;
        this.animation.Play();
    }
}