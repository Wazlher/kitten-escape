﻿using UnityEngine;
using System.Collections;

public class UseWeapon : MonoBehaviour
{
    public Camera               CameraMain;
    public Camera               CameraFPS;
    public GameObject           bullet_prefab;
    public Transform            Spawn;
    public float                bulletImpulse = 2000.0f;

    private GUI_WeaponUnlock    weaponUnlock;

    void Start()
    {
        CameraMain.enabled = true;
        CameraFPS.enabled = false;
        weaponUnlock = GameObject.FindGameObjectWithTag("GUI").GetComponent<GUI_WeaponUnlock>();
    }

    void Update()
    {
        if (this.GetComponent<controller_player>().move)
        {
            if (Input.GetKeyDown(KeyCode.X) && weaponUnlock.isUnlocked)
            {
                if (animation.isPlaying)
                    animation.Stop();
                CameraMain.enabled = !CameraMain.enabled;
                CameraFPS.enabled = !CameraFPS.enabled;
            }

            if (Input.GetMouseButtonDown(0) && CameraFPS.enabled) 
            {
                GameObject thebullet = Instantiate(this.bullet_prefab, Spawn.position, Spawn.rotation) as GameObject;
                thebullet.rigidbody.AddForce(Spawn.forward * bulletImpulse);
            }
        }
    }
}