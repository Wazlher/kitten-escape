﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MiniMap : MonoBehaviour {
	
	public Transform character;
	public Transform cube;
	
	void Start()
	{
        cube.renderer.castShadows = false;
	}
	
	void LateUpdate () {
        transform.position = new Vector3(character.position.x, transform.position.y, character.position.z);
        cube.position = new Vector3(character.position.x, cube.position.y, character.position.z);
        cube.rotation = character.rotation;
	}
}
