﻿using UnityEngine;
using System.Collections;

public class GUI_KittyCatched : MonoBehaviour
{
    public Texture              kittyIcon;
	GameSystem					GS;

    private int                 kittyToWeapon;
    private GUI_WeaponUnlock    weaponUnlock;
    private bool                weaponCheck = false;

    void Awake()
    {
        GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
        kittyToWeapon = GS.kittenTotal / 5;
        weaponUnlock = GameObject.FindGameObjectWithTag("GUI").GetComponent<GUI_WeaponUnlock>();
    }

    void OnGUI ()
    {
        GUILayout.BeginArea(new Rect(Screen.width - 200, 0, 200, 73));
        GUILayout.BeginHorizontal();
        GUILayout.TextField(GS.kittenCatched.ToString() + " / " + GS.kittenTotal.ToString());
        GUILayout.Label(kittyIcon);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    void Update()
    {
        if (GS.kittenCatched >= kittyToWeapon && !weaponCheck)
        {
            weaponUnlock.isUnlocked = true;
            weaponCheck = true;
        }
    }
}
