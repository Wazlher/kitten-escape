﻿using UnityEngine;
using System.Collections;

public class GUI_WeaponUnlock : MonoBehaviour
{
    public Texture weaponIcon;
    public string activationKey;
    public bool isUnlocked = false;

    void OnGUI()
    {
        if (isUnlocked)
        {
            GUI.Label(new Rect(5, 0, 73, 73), weaponIcon);
            GUI.Label(new Rect(5, 65, 110, 20), "Press " + activationKey + " to use");
        }
    }
}
