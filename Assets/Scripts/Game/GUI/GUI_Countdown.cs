﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GUI_Countdown : MonoBehaviour
{
    public float timeLeft;
	private bool isFinished = false;
	public bool start = false;

    void OnGUI()
    {
        int minutes = Mathf.FloorToInt(timeLeft / 60f);
        int seconds = Mathf.FloorToInt(timeLeft - minutes * 60);
        string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);

        GUI.TextField(new Rect(Screen.width / 2, 0, 40, 20), niceTime);
    }

    public void Update()
    {
		if (!start)
			return;
        if (timeLeft > 0.0f)
            timeLeft -= Time.deltaTime;
		if (timeLeft <= 0.0f)
		{
			if (!isFinished)
			{
				GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
				if (GS != null)
					GS.StartEndOfGame(false);
				isFinished = true;
			}
			timeLeft = 0.0f;
		}	
    }
}