﻿using UnityEngine;
using System.Collections;

public class Ovni : MonoBehaviour {
	
	private bool endOn = false;
		
	void OnTriggerEnter(Collider other)
	{
		if (!endOn && other.tag == "Player")
		{
			GameSystem GS = GameObject.Find("GameSystem").GetComponent("GameSystem") as GameSystem;
			if (GS.kittenCatched == GS.kittenTotal)
			{
				GS.StartEndOfGame(true);
				endOn = true;
			}
		}
	}
}
